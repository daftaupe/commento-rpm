%define debug_package %{nil}
%define repo gitlab.com/commento/commento

Name:           commento
Version:        1.8.0
Release:        1%{?dist}
Summary:        A privacy-focused and bloat-free Disqus alternative

License:        MIT
URL:            https://%{repo}
Source0:        https://%{repo}/-/archive/v%{version}/commento-v%{version}.tar.gz

BuildRequires:  bash go npm git yarn 
%if 0%{?fedora}
BuildRequires: dep systemd
%endif

AutoReq:        no 
AutoReqProv:    no

%description
A privacy-focused and bloat-free Disqus alternative.

%prep
%setup -q -c -n %{name}-v%{version}

%build
mkdir -p src/gitlab.com/commento
mv %{name}-v%{version} src/gitlab.com/commento/commento
export GOPATH=$(pwd)
export PATH=$PATH:$GOPATH/bin
go get -u github.com/golang/dep/cmd/dep
cd src/gitlab.com/commento/commento
export PATH=$PATH:$(pwd)/node_modules/.bin
npm install uglify-js html-minifier sass
make prod

%install
mkdir -p %{buildroot}%{_bindir}
mkdir -p %{buildroot}%{_unitdir}
mkdir -p %{buildroot}%{_datadir}
mkdir -p %{buildroot}%{_sysconfdir}/%{name}

cp src/gitlab.com/commento/commento/etc/linux-systemd/%{name}.service %{buildroot}%{_unitdir}
mv src/gitlab.com/commento/commento/build/prod/%{name} %{buildroot}%{_bindir}
cp -r src/gitlab.com/commento/commento/build/prod %{buildroot}%{_datadir}/%{name}

%pre
getent group commento >/dev/null || groupadd -r commento
getent passwd commento >/dev/null || \
    useradd -r -g commento -d /var/lib/commento -s /sbin/nologin \
    -c "commento" commento
exit 0


%files
%{_bindir}/%{name}
%{_datadir}/%{name}
%{_unitdir}/%{name}.service
%license src/gitlab.com/commento/commento/LICENSE

%changelog
* Wed Apr 15 2020 Pierre-Alain TORET <pierre-alain.toret@protonmail.com> 1.8.0-1
- Update to version 1.8.0

* Thu May 02 2019 Pierre-Alain TORET <pierre-alain.toret@protonmail.com> 1.7.0-1
- Update to version 1.7.0

* Thu Feb 21 2019 Pierre-Alain TORET <pierre-alain.toret@protonmail.com> 1.6.2-1
- Update to version 1.6.2

* Sun Feb 10 2019 Pierre-Alain TORET <pierre-alain.toret@protonmail.com> 1.5.0-1
- Update to version 1.5.0

* Sun Oct 21 2018 Pierre-Alain TORET <pierre-alain.toret@protonmail.com> 1.3.1-1
- Update to version 1.3.1

* Thu Jun 14 2018 Pierre-Alain TORET <pierre-alain.toret@protonmail.com> r123.e937335d-1
- Initial version r123.e937335d
